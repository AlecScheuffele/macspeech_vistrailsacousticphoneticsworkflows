import wave
import os
import subprocess
import tgt as tgt




class Formant(object):

    #formantData: -> [-------------------------------------------------------------------------------------------------]
    #                 [-----timestamp1-----]  , [--------f1--------] , [--------f2--------] , [--------f3--------]]
    #                   val1, val2, val3...
    #

    def __init__(self, formantData):
        self.timestamp = formantData[0]
        self.f1 = formantData[1]
        self.f2 = formantData[2]
        self.f3 = formantData[3]

    def __str__(self):
        return self.getInfo()

    def getInfo(self):
        result = "".join([str("\n" + '%-5s%-20s%-10s%-10s%-8s' % ("",   y[0],                 y[1],        y[2],         y[3]))        for y in     \
                                                                         [["Time:",           "f1:",       "f2:",        "f3:"]] +                  \
                                                                         [[self.timestamp[x],  self.f1[x],   self.f2[x],   self.f3[x]]              \
                                                                         for x in range(0,len(self.timestamp))]])
        return result

    def get_timestamp(self):
        return self.timestamp
    def get_f1(self):
        return self.f1
    def get_f2(self):
        return self.f2
    def get_f3(self):
        return self.f3





class Frequency(object):

    #formantData: -> [------------------------------------------------]
    #                 [-----timestamp1-----]  , [--------f0--------]  ]
    #                   val1, val2, val3...
    #

    def __init__(self, formantData):
        self.timestamp = formantData[0]
        self.f0 = formantData[1]


    def __str__(self):
        return self.getInfo()

    def getInfo(self):
        result = "".join([str("\n" + '%-22s%-16s%-18s' % ("",   y[0],                 y[1]        ))        for y in     \
                                                                [["Time",             "f0"        ]] +                   \
                                                                [[self.timestamp[x],  self.f1[x]   ]                     \
                                                                for x in range(0,len(self.timestamp))]])
        return result

    def get_timestamp(self):
        return self.timestamp
    def get_f1(self):
        return self.f0







def MapFormantsWRASSP(wavPath, startTime=None, endTime=None):
    #----------------Wav Meta----------------
    wav = wave.open(wavPath, "rb")
    sampleRate = wav.getframerate()
    numberOfSamples = wav.getnframes()
    duration = float((float(numberOfSamples)/float(sampleRate)))            # ^ ^ ^ ^ all this for getting the duration

    if startTime is None:
        startTime = 0
    if endTime is None:
        endTime = duration

    #output val:
    #vals:             0                          1                     3............        (3 + numberOfFormantSlices)  ....
    #      |--------------------------|--------------------------|--------------------------|---------------------------|
    #        number of formant slices     formant slice rate            f1  values                      f2 values

    scriptDir = 'R_Scripts/formTest.R'
    output = subprocess.check_output(['Rscript',        # R output string
                                      scriptDir,
                                      wavPath,
                                      str(startTime),
                                      str(endTime)]).split(' ', 2)

    numberOfFormantSlices = int(output[0])
    formantSliceRate = float(output[1])
    formantSliceDuration = 1/formantSliceRate
    dataList = map(int, output[2].split())                                          # consecutive list of all output formant values -- unstructured

    timestamps = []                                                                 # list of timestamps for each formant slice
    formantData = []                                                                # (structured) 2D list of formant data of length: [number of formants + 1] --- (+1 is for timestamps)

    for i in range(0, numberOfFormantSlices):
        timestamps.append(formantSliceDuration*i + formantSliceDuration/2)          #get time-stamp value at center of formantSlice
    formantData.append(timestamps)                                                  #add timestamps to formantData

    numberOfFormants = 3
    for i in range(0, numberOfFormants+2):                                          # get formants(i = 1, 2, ...numberOfFormants) in dataList
        start = i * numberOfFormantSlices
        end = (i + 1) * numberOfFormantSlices
        formantData.append(dataList[start:end])
    return Formant(formantData)                                                              #RETURN







def MapFrequencyWRASSP(wavPath, startTime=None, endTime=None):
    #----------------Wav Meta----------------
    wav = wave.open(wavPath, "rb")
    sampleRate = wav.getframerate()
    numberOfSamples = wav.getnframes()
    duration = float((float(numberOfSamples)/float(sampleRate)))            # ^ ^ ^ ^ all this for getting the duration

    if startTime is None:
        startTime = 0
    if endTime is None:
        endTime = duration

    scriptDir = 'R_Scripts/freqTest.R'
    output = subprocess.check_output(['Rscript',          # R output string
                                      scriptDir,
                                      wavPath,
                                      str(startTime),
                                      str(endTime)]).split(' ', 2)

    numberOfFreqSlices = int(output[0])
    freqSliceRate = float(output[1])
    freqSliceDuration = 1/freqSliceRate
    freqList = map(float, output[2].split())                                  # consecutive list of all output freq values -- unstructured

    timestamps = []                                                           # list of timestamps for each freq slice
    freqData = []                                                             # 2D array of frequency data --- length: 2 --- (timestamps and freqList)

    for i in range(0, numberOfFreqSlices):
        timestamps.append(freqSliceDuration*i + freqSliceDuration/2)          #get time-stamp value at center of formantSlice      #TODO: speed up with numpy
    freqData.append(timestamps)                                               #add timestamps to freqData
    freqData.append(freqList)                                                 #add freqList to freqData
    return freqData






def pullIntervalsFromTimeline(intervalValues, timestampValues, attributeValues):
    #intervalValues [start, end]
    #timestampValues [timestamp_1, ... , timestamp_n]
    #attributeValues [attribute_1, ... , attribute_n]
    measurements = []
    for iterator in range(0, len(timestampValues)):
        if (timestampValues[iterator] > intervalValues[0] and timestampValues[iterator] < intervalValues[1]):
            measurements.append(attributeValues[iterator])
    result = measurements
    return result






def pullIntervalsFromTimelineAverage(intervalValues, timestampValues, attributeValues):
    #intervalValues [start, end]
    #timestampValues [timestamp_1, ... , timestamp_n]
    #attributeValues [attribute_1, ... , attribute_n]
    measurements = []
    for iterator in range(0, len(timestampValues)):
        if (timestampValues[iterator] > intervalValues[0] and timestampValues[iterator] < intervalValues[1]):
            measurements.append(attributeValues[iterator])
    result = sum(measurements) / float(len(measurements))
    return result





#returns token distribution (token -> prevelence)
def getTokenDistribution(directory, textgrid_Name_List, tierName, targets=None):
    distributionMap = {}
    for textGrid in textgrid_Name_List:
        targetTier = tgt.read_textgrid(directory+textGrid+'.TextGrid').get_tier_by_name(tierName)
        tokensPresent = [interval.text for interval in targetTier._get_annotations()]
        for token in tokensPresent:
            if token in distributionMap:
                distributionMap[token] = distributionMap[token]+1
            else:
                distributionMap[token] = 1

    if not(targets is None):                                                    #remove tokens that are not target tokens
        targetedDistributionMap = {}
        for token in distributionMap:
            if token in targets:
                targetedDistributionMap[token] = distributionMap[token]
        return targetedDistributionMap
    else:
        return distributionMap



def getTokenDuration(directory, textgrid_Name_List, tierName, targets=None):
    durationMap = {}                    #token -> [duration1, duration2, duration3]
    for textGrid in textgrid_Name_List:
        tier = tgt.read_textgrid(directory+textGrid+'.TextGrid').get_tier_by_name(tierName)
        targetsPresent = set([interval.text for interval in tier._get_annotations()]) & set(targets)
        for target in targetsPresent:
            for annotation in tier.get_annotations_with_text(pattern=target, n=0, regex=False):
                duration = float(annotation._end_time) - float(annotation._start_time)
                if not(target in durationMap):
                    durationMap[target] = [duration]
                else:
                    durationMap[target].append(duration)
    for token in durationMap:
        durationMap[token] = sum(durationMap[token]) / float(len(durationMap[token]))
    return durationMap





# foctor is the number of "time-steps" for a formant profile
#[ phone, [value1, value2...] ]
def normalizeFormantTimeSeries(formantValues, factor):
        data = formantValues                             #[value1, value2, value3...valueN]
        factor = factor                                       #[normalizedValue1, normalizedValue2.... normalizedValueN]
        normalizedData = []
        ratio = float(len(data)-1)/float(factor-1)
        start = data[0]
        end = data[len(data)-1]
        normalizedData.append(start)
        for iterator in range(1,factor-1):
            index = iterator*ratio
            valueRange = data[min(len(data)-1,int(index)+1)] - data[int(index)]     #high - low
            # print("high: " + str(data[min(len(data)-1,int(index)+1)]) + "low: " + str(data[int(index)]))
            fraction = index - int(index)
            value = (valueRange*fraction) + data[int(index)]                        # frac + low
            normalizedData.append(value)
        normalizedData.append(end)
        return normalizedData







def MAUS(path, fileName, word):
# use MAUS to segment acoustic signals, find vowel segments
    segments = []
    count = 0

    tgfile = path + fileName.split('.')[0] + '2.TextGrid'
    textgrid = maus.annotate_wav(path + fileName, word, language='aus')
    with open(tgfile, 'w') as out:
        out.write(textgrid)































































# class Timeline(object):
#     #   data: -> [-------------------------------------------------------------------------------------------------]
#     #               [-----timestamp1-----]  , [--------f1--------] , [--------f2--------] , [--------f3--------]]
#     #                 val1, val2, val3...
#     #
#     def __init__(self, data):
#         self.timestamp = formantData[1]
#         #attributes
#         self.f1 = formantData[2]
#         self.f2 = formantData[3]
#         self.f3 = formantData[4]



