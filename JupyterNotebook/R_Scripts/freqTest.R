library(wrassp)

get_Pitch <- function(segments, wavmap, startTime, endTime) {
    res = ksvF0(wavmap, beginTime=startTime, endTime=endTime, toFile=FALSE)

    numberOfFreqSlices = NROW(res[[1]])
    freqData = res[[1]]
    result = cat(numberOfFreqSlices, attributes(res)$sampleRate,freqData)
}

args = commandArgs(TRUE)
if (length(args) != 3) {
    print("Wrong number of arguments!")
    exit(1)
}

wavfile = args[1]
startTime = as.numeric(args[2])
endTime = as.numeric(args[3])
get_Pitch(segments, wavfile, startTime, endTime)