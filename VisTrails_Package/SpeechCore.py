import wave
import os
import subprocess




class Formant(object):
    #formantData: -> [-------------------------------------------------------------------------------------------------]
    #                 [-----timestamp1-----]  , [--------f1--------] , [--------f2--------] , [--------f3--------]]
    #                   val1, val2, val3...
    #
    def __init__(self, formantData):
        self.timestamp = formantData[0]
        self.f1 = formantData[1]
        self.f2 = formantData[2]
        self.f3 = formantData[3]

    def pullIntervalsFromTimeline(self, startTime, endTime):
        f1_pulled = []
        f2_pulled = []
        f3_pulled = []
        for i in range(0, len(self.timestamp)):
            if (self.timestamp[i] > startTime and self.timestamp[i] < endTime):
                f1_pulled.append(self.f1[i])
                f2_pulled.append(self.f2[i])
                f3_pulled.append(self.f3[i])
        return [f1_pulled, f2_pulled, f3_pulled]

    def get_timestamp(self):
        return self.timestamp
    def get_f1(self):
        return self.f1
    def get_f2(self):
        return self.f2
    def get_f3(self):
        return self.f3




#------------------------------------function to normalize trajectories------------------------------------------------
def normalizeFormantTimeSeries_Interpolate(formantValues, factor):
    result = []
    offsetRatio = float(len(formantValues)-1)/float(factor-1)
    for i in range(0,factor):
        offsetIndex = i*offsetRatio
        valueRange = formantValues[min(len(formantValues)-1,int(offsetIndex)+1)] - formantValues[int(offsetIndex)]
        offsetAmount = offsetIndex - int(offsetIndex)
        result.append(formantValues[int(offsetIndex)] + (valueRange*offsetAmount))
        #value     =                  ---index---    +    ---offset amount---
    return result



#---------------------------------------map formants---------------------------------------
def getFormantsWRASSP(wavPath, startTime, endTime):
    formantData = []
    scriptDir = str(os.path.dirname(os.path.realpath(__file__))) + '/R_Scripts/formTest.R'
    print ('Rscript', scriptDir, wavPath, str(startTime), str(endTime))
    output = subprocess.check_output(['Rscript',
                                      scriptDir,
                                      wavPath,
                                      str(startTime),
                                      str(endTime)],
                                      shell=False).split(' ', 2)
    numberOfFormantSlices = int(output[0])
    formantSliceDuration = 1/float(output[1])
    dataList = map(int, output[2].split())
    numberOfFormants = 3
    formantData.append([formantSliceDuration*i + formantSliceDuration/2 for i in range(0, numberOfFormantSlices)])  #timestamps
    formantData.extend([dataList[i*numberOfFormantSlices : (i+1)*numberOfFormantSlices] for i in range(0, numberOfFormants+2)])
                                    #-----start------       #----------end-----------
    return Formant(formantData)





#--------------------------getting phone formant trajectories-------------------------------
def getPhoneIntervals_fromTextgrid(textgrid, target, phoneTier):
    vowelTier = textgrid.get_tier_by_name('PHN')
    vowelAnnotations = vowelTier.get_annotations_with_text(pattern=target, n=0, regex=False)
    intervals = [[float(annotation._start_time), float(annotation._end_time)] for annotation in vowelAnnotations]
    return intervals


def getPhoneTrajectory_fromTextgrid(textgrid, formantMap, target, phoneTier):
    return [formantMap.pullIntervalsFromTimeline(interval[0], interval[1])
            for interval in getPhoneIntervals_fromTextgrid(textgrid, target, phoneTier)]




#------------------------------------function to normalize trajectories------------------------------------------------
def normalizeFormantTimeSeries_Interpolate(formantValues, factor):
    result = []
    if len(formantValues) is 0:
        result = [0 for x in range(0, factor)]
    else:
        offsetRatio = float(len(formantValues)-1)/float(factor-1)
        for i in range(0,factor):
            offsetIndex = i*offsetRatio
            valueRange = formantValues[min(len(formantValues)-1,int(offsetIndex)+1)] - formantValues[int(offsetIndex)]
            offsetAmount = offsetIndex - int(offsetIndex)
            result.append(formantValues[int(offsetIndex)] + (valueRange*offsetAmount))
            #value     =                  ---index---    +    ---offset amount---
    return result



#--------------------------------function to train classifier (round robbin)---------------------------------
def roundRobbin(dataFrame, factorID, dataIn, dataOut, classifierConstructor):
    factorValues = dataFrame[factorID].unique().tolist()
    classifiers = [classifierConstructor for factor in factorValues]
    results = []
    for i in range(0,len(factorValues)):
        trainingData = [list(dataFrame[dataFrame[factorID] != factorValues[i]][dataIn]), list(dataFrame[dataFrame[factorID] != factorValues[i]][dataOut])]
        testData = [list(dataFrame[dataFrame[factorID] == factorValues[i]][dataIn]), list(dataFrame[dataFrame[factorID] == factorValues[i]][dataOut])]
        classifiers[i].fit(trainingData[0], trainingData[1])
        predictions = classifiers[i].predict(testData[0])
        results.append([predictions, testData[1]])
    return results



def buildConfusionMatrix(result, expected, targetPhones):
    print(result)
    print(expected)
    predictionMatrixIndexing = {}
    confusionMatrix = [[0 for x in range(0, len(targetPhones))] for x in range(0, len(targetPhones))]
    targetPhonesList = list(targetPhones)
    for index in range(0, len(targetPhonesList)):
        predictionMatrixIndexing[targetPhonesList[index]] = index
    for i in range(0,len(result)):
        if expected[i] in predictionMatrixIndexing and result[i] in predictionMatrixIndexing:
            confusionMatrix[predictionMatrixIndexing[expected[i]]][predictionMatrixIndexing[result[i]]] += 1
        #else: ignore, not a target phone
    return [targetPhones, confusionMatrix]







#returns token distribution (token -> prevelence)
def getTokenOccurrence(directory, textgrid_Name_List, tierName, targets=None):
    OccurrenceMap = {}
    for textGrid in textgrid_Name_List:
        targetTier = tgt.read_textgrid(directory+textGrid+'.TextGrid').get_tier_by_name(tierName)
        tokensPresent = [interval.text for interval in targetTier._get_annotations()]
        for token in tokensPresent:
            if token in OccurrenceMap:
                OccurrenceMap[token] = OccurrenceMap[token]+1
            else:
                OccurrenceMap[token] = 1

    if not(targets is None):                                                    #remove tokens that are not target tokens
        targetedDistributionMap = {}
        for token in OccurrenceMap:
            if token in targets:
                targetedDistributionMap[token] = OccurrenceMap[token]
        return targetedDistributionMap
    else:
        return OccurrenceMap




def getTokenDuration(directory, textgrid_Name_List, tierName, targets=None):
    durationMap = {}                                                           #token -> [duration1, duration2, duration3]
    for textGrid in textgrid_Name_List:
        tier = tgt.read_textgrid(directory+textGrid+'.TextGrid').get_tier_by_name(tierName)
        targetsPresent = set([interval.text for interval in tier._get_annotations()]) & set(targets)
        for target in targetsPresent:
            for annotation in tier.get_annotations_with_text(pattern=target, n=0, regex=False):
                duration = float(annotation._end_time) - float(annotation._start_time)
                if not(target in durationMap):
                    durationMap[target] = [duration]
                else:
                    durationMap[target].append(duration)
    for token in durationMap:
        durationMap[token] = sum(durationMap[token]) / float(len(durationMap[token]))
    return durationMap







