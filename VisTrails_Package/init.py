########################################################################################
# MacSpeech --- Exploratory VisTrails Modules For building Acoustic Phonetics Workflows

from vistrails.core.modules.vistrails_module import Module, ModuleError
from vistrails.core.modules.config import IPort, OPort, ModuleSettings, ConstantWidgetConfig, QueryWidgetConfig, ParamExpWidgetConfig
from vistrails.core.modules.basic_modules import Constant, List
import wave
import os
import subprocess
import numpy as np
from vistrails.packages.matplotlib import plots, figure_cell
from vistrails.packages.spreadsheet.basic_widgets import SpreadsheetCell, SpreadsheetMode
import pandas as pd
import tgt as tgt
import SpeechCore as speech
from scipy import fftpack
from sklearn.naive_bayes import GaussianNB
import matplotlib.pyplot as plt
from copy import deepcopy


#*****************************************************************************
#-----------------------------------------------------------------------------
#----------------------New Types----------------------------------------------
#-----------------------------------------------------------------------------
#*****************************************************************************

class PandasDataFrame(Constant):

    def __init__(self):
        Module.__init__(self)

    def is_cacheable(self):
        return True

    @staticmethod
    def translate_to_python(x):
        return x
        print(x)


class VistrailsTextGrid(Constant):

    def __init__(self):
        Module.__init__(self)


    def is_cacheable(self):
        return True

    @staticmethod
    def translate_to_python(x):
        return x
        print(x)


class TextGrid(Module):
    _input_ports = [IPort(name="Python TextGridObject", signature="TextGrid")]
    _output_ports = [OPort(name="VisTrails TextGridObject", signature="TextGrid")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        self.set_output("VisTrails TextGridObject", self.get_input("Python TextGridObject"))












#*****************************************************************************
#-----------------------------------------------------------------------------
#--------------------DataFrame_Operations-------------------------------------
#-----------------------------------------------------------------------------
#*****************************************************************************


class Create_dFrame(Module):
    _input_ports = [IPort(name="column_names", signature="basic:List"),
                    IPort(name="column_values", signature="basic:List")]
    _output_ports = [OPort(name="result_dFrame", signature="PandasDataFrame")]

    def compute(self):
        column_names = self.get_input("column_names")
        column_values = self.get_input("column_values")

        result_dFrame = pd.DataFrame()
        for i in range(0,len(column_names)):
            column = pd.Series(column_values[i])
            result_dFrame[column_names[i]] = column
        self.set_output("result_dFrame", result_dFrame)


class dFrame_toRows(Module):
    _input_ports = [IPort(name="dFrame", signature="PandasDataFrame")]
    _output_ports = [OPort(name="rows", signature="basic:List")]

    def compute(self):
        dFrame = self.get_input("dFrame")
        result_dFrame = []
        for index, row in dFrame.iterrows():
            result_dFrame.append(row)
        self.set_output("rows", result_dFrame)


class row_to_list(Module):
    _input_ports = [IPort(name="row", signature="basic:Variant")]
    _output_ports = [OPort(name="rowList", signature="basic:List")]

    def compute(self):
        self.set_output("rowList", self.get_input("row").tolist())


class dFrame_row_element(Module):
    _input_ports = [IPort(name="row", signature="basic:Variant"),
                    IPort(name="columnName", signature="basic:String")]
    _output_ports = [OPort(name="rows", signature="basic:Variant")]

    def compute(self):
        self.set_output("rows", self.get_input("row")[self.get_input("columnName")])


class merge_dFrame(Module):
    _input_ports = [IPort(name="dFrame_left", signature="PandasDataFrame"),
                    IPort(name="dFrame_right", signature="PandasDataFrame"),
                    IPort(name="mergetype", signature="basic:String"),
                    IPort(name="left_on", signature="basic:String"),
                    IPort(name="right_on", signature="basic:String")]
    _output_ports = [OPort(name="result_dFrame", signature="PandasDataFrame")]

    def compute(self):
        dFrame_left = self.get_input("dFrame_left")
        dFrame_right = self.get_input("dFrame_right")
        mergetype = self.get_input("mergetype")
        left_on_index = self.get_input("left_on")
        right_on_index = self.get_input("right_on")
        merged_dFrame = pd.merge(dFrame_left,
                                 dFrame_right,
                                 left_on=left_on_index,
                                 right_on=right_on_index,
                                 how=mergetype)
        self.set_output("result_dFrame", merged_dFrame)


class Query_dFrame(Module):
    _input_ports = [IPort(name="in_dFrame", signature="PandasDataFrame"),
                    IPort(name="query_string", signature="basic:String")]
    _output_ports = [OPort(name="out_dFrame", signature="PandasDataFrame")]

    def compute(self):
        self.set_output("out_dFrame", self.get_input("in_dFrame").query(self.get_input("query_string")))


class dFrame_addColumn(Module):
    _input_ports = [IPort(name="column_name", signature="basic:String"),
                    IPort(name="column_values", signature="basic:List"),
                    IPort(name="dFrame", signature="PandasDataFrame")]
    _output_ports = [OPort(name="result_dFrame", signature="PandasDataFrame")]

    def compute(self):
        result_dFrame = self.get_input("dFrame")
        result_dFrame[self.get_input("column_name")] = pd.Series(self.get_input("column_values"))
        self.set_output("result_dFrame", result_dFrame)


class dFrame_getUnique(Module):
    _input_ports = [IPort(name="in_pd", signature="PandasDataFrame"),
                    IPort(name="columnID", signature="basic:String")]
    _output_ports = [OPort(name="out_list", signature="basic:List")]

    def compute(self):
        in_pd = self.get_input("in_pd")
        out_list = in_pd[self.get_input("columnID")].unique().tolist()
        self.set_output("out_list", out_list)


class dFrame_getColumn(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="columnName", signature="basic:String")]
    _output_ports = [OPort(name="list", signature="basic:List")]


    def compute(self):
        self.set_output("list", self.get_input("dataFrame")[self.get_input("columnName")].tolist())














#*****************************************************************************
#-----------------------------------------------------------------------------
#--------------------------------List_Operations------------------------------
#-----------------------------------------------------------------------------
#*****************************************************************************


class Length(Module):
    _input_ports = [IPort(name="listIn", signature="basic:List")]
    _output_ports = [OPort(name="result", signature="basic:Integer")]

    def compute(self):
        self.set_output("result", len(self.get_input("listIn")))


class Append(Module):
    _input_ports = [IPort(name="list1", signature="basic:List"),
                    IPort(name="new_element", signature="basic:Variant")]
    _output_ports = [OPort(name="appendedList", signature="basic:List")]

    def compute(self):
        result = deepcopy(self.get_input("list1"))
        result.append(self.get_input("new_element"))
        self.set_output("appendedList", result)


class distribute(Module):
    _input_ports = [IPort(name="element", signature="basic:Variant"),
                    IPort(name="targetList", signature="basic:List")]
    _output_ports = [OPort(name="result", signature="basic:List")]

    def compute(self):
        element = self.get_input("element")
        targetList = self.get_input("targetList")
        self.set_output("result",[x + [element] for x in targetList])


class Extend(Module):
    _input_ports = [IPort(name="size", signature="basic:Integer"),
                    IPort(name="value", signature="basic:Variant")]
    _output_ports = [OPort(name="list", signature="basic:List")]

    def compute(self):
        self.set_output("list", [self.get_input("value") for x in range(0,self.get_input("size"))])



class flatten(Module):
    _input_ports = [IPort(name="inList", signature="basic:List")]
    _output_ports = [OPort(name="outList", signature="basic:List")]

    def compute(self):
        inList = self.get_input("inList")
        self.set_output("outList", [x for y in inList for x in y])


class ZipList(Module):
    _input_ports = [IPort(name="listIn", signature="basic:List")]
    _output_ports = [OPort(name="listOut", signature="basic:List")]

    def compute(self):
        self.set_output("listOut", zip(*self.get_input("listIn")))


class removeElement(Module):
    _input_ports = [IPort(name="inputList", signature="basic:List"),
                    IPort(name="targetElement", signature="basic:Integer")]
    _output_ports = [OPort(name="outputList", signature="basic:List")]

    def compute(self):
        element = self.get_input("targetElement")
        self.set_output("outputList", [x for x in self.get_input("inputList") if (not(x == element))])


class normalizeList(Module):
    _input_ports = [IPort(name="inList", signature="basic:List"),
                    IPort(name="factor", signature="basic:Integer")]
    _output_ports = [OPort(name="outList", signature="basic:List")]

    def compute(self):
        inList = self.get_input("inList")
        normalizationFactor = self.get_input("factor")
        self.set_output("outList", speech.normalizeFormantTimeSeries_Interpolate(inList, normalizationFactor))


class Concat(Module):
    _input_ports = [IPort(name="list1", signature="basic:List"),
                    IPort(name="list2", signature="basic:List")]
    _output_ports = [OPort(name="concatList", signature="basic:List")]

    def compute(self):
        result = deepcopy(self.get_input("list1"))
        result = result + self.get_input("list2")
        self.set_output("concatList", result)












#*****************************************************************************
#-----------------------------------------------------------------------------
#--------------------------------TextGrid_Operations--------------------------
#-----------------------------------------------------------------------------
#*****************************************************************************


class get_annotations(Module):
    _input_ports = [IPort(name="tier", signature="basic:String"),
                    IPort(name="TextGrid", signature="TextGrid")]
    _output_ports = [OPort(name="annotations", signature="basic:List")]

    def compute(self):
        self.set_output("annotations", self.get_input("TextGrid").get_tier_by_name(self.get_input("tier"))._get_annotations())



class get_interval(Module):
    _input_ports = [IPort(name="Annotation", signature="basic:Variant")]
    _output_ports = [OPort(name="Text", signature="basic:String"),
                     OPort(name="Interval", signature="basic:List")]

    def compute(self):
        annotation = self.get_input("Annotation")
        self.set_output("Text", annotation.text)
        self.set_output("Interval", [float(annotation._start_time), float(annotation._end_time)])


class removeElement(Module):
    _input_ports = [IPort(name="inputList", signature="basic:List"),
                    IPort(name="targetElement", signature="basic:Integer")]
    _output_ports = [OPort(name="outputList", signature="basic:List")]

    def compute(self):
        element = self.get_input("targetElement")
        self.set_output("outputList", [x for x in self.get_input("inputList") if (not(x == element))])











#*****************************************************************************
#-----------------------------------------------------------------------------
#-----------------------------------Misc--------------------------------------
#-----------------------------------------------------------------------------
#*****************************************************************************


class compare(Module):
    _input_ports = [IPort(name="val1", signature="basic:Variant"),
                    IPort(name="op", signature="basic:String"),
                    IPort(name="val2", signature="basic:Variant")]
    _output_ports = [OPort(name="boolOut", signature="basic:Boolean")]

    def compute(self):
        val1 = self.get_input("val1")
        val2 = self.get_input("val2")
        op = self.get_input("op")
        boolOut = False
        if (op == ">"):
            if (float(val1) > float(val2)):
                boolOut = True
        if (op == "<"):
            if (float(val1) < float(val2)):
                boolOut = True
        if (op == "="):
            if (val1 == val2):
                boolOut = True
        self.set_output("boolOut", boolOut)



class prepareInputData(Module):
    _input_ports = [IPort(name="workspacePath", signature="basic:String"),
                    IPort(name="speakers", signature="basic:List")]
    _output_ports = [OPort(name="textgrid_dFrame", signature="PandasDataFrame")]

    #--------------------------------function to create textGrid from label file------------------------------
    def labToTextgrid(self, labFile_fileObject, ID=""):
        labFileData = [str(line).splitlines()[0].split("\t") for line in labFile_fileObject.readlines()]
        newTextGrid = tgt.TextGrid(ID)
        phnTier = tgt.IntervalTier(name="PHN")
        startTimeIndex = 0
        for stamp in range(3,len(labFileData)):          #---start---                ---end---                 ---phone---
            phnTier.add_annotation(tgt.Annotation(float(startTimeIndex), float(labFileData[stamp][1]), str(labFileData[stamp][3])))
            startTimeIndex = labFileData[stamp][1]
        newTextGrid.add_tier(phnTier)
        return(newTextGrid)

    def compute(self):
        speakers = self.get_input("speakers")
        workspacePath = self.get_input("workspacePath")
        textgrid_dFrame = pd.DataFrame([], columns=['ID', 'Speaker', 'TextGrid', 'WavPath'])
        for speaker in speakers:
            labFileDirectory = workspacePath+"/"+speaker+"/labels"
            labelFileNames = [n[0]+"."+n[1]+"."+n[2] for n in [f.split(".") for f in os.listdir(labFileDirectory)] if n[-1] == 'lab']
            for labFile in labelFileNames:
                ID = labFile.split('.lab')[0]
                textGrid = self.labToTextgrid(open(labFileDirectory+"/"+labFile, "r"), ID)       #save textgrid
                wavpath = workspacePath+"/"+speaker+"/"+"wavs"+"/"+ID+".wav"
                textgrid_dFrame = textgrid_dFrame.append(pd.DataFrame({'ID':[ID], 'Speaker':[speaker], 'TextGrid':[textGrid], 'WavPath':[wavpath]}))
        self.set_output("textgrid_dFrame", textgrid_dFrame)












#********************************************************************************************
#--------------------------------------------------------------------------------------------
#----------------------------------------Data Operations-------------------------------------
#--------------------------------------------------------------------------------------------
#********************************************************************************************


class getIntervals(Module):
    _input_ports = [IPort(name="tier", signature="basic:String"),
                    IPort(name="TextGrid_Column", signature="basic:String"),
                    IPort(name="dFrame", signature="PandasDataFrame")]
    _output_ports = [OPort(name="result_dFrame", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        df = self.get_input("dFrame")
        tier = self.get_input("tier")
        textgridColumn = self.get_input("TextGrid_Column")
        build_dFrame = pd.DataFrame()
        for index, row in df.iterrows():
            textgrid = row[textgridColumn]
            annotations = textgrid.get_tier_by_name(tier)._get_annotations()
            intervals = [[annotation.text, [float(annotation._start_time), float(annotation._end_time)]] for annotation in annotations]
            for interval in intervals:
                build_dFrame = build_dFrame.append(pd.DataFrame({textgridColumn: [textgrid], tier: [interval[0]], 'Interval': [interval[1]]}))
        merged_dFrame = pd.merge(df,
                                 build_dFrame,
                                 left_on=textgridColumn,
                                 right_on=textgridColumn,
                                 how='left')
        self.set_output("result_dFrame", merged_dFrame)


class getTextgridStartFinishIntervals(Module):
    _input_ports = [IPort(name="tier", signature="basic:String"),
                    IPort(name="TextGrid_Column", signature="basic:String"),
                    IPort(name="dFrame", signature="PandasDataFrame")]
    _output_ports = [OPort(name="result_dFrame", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        df = self.get_input("dFrame")
        tier = self.get_input("tier")
        textgridColumn = self.get_input("TextGrid_Column")
        build_dFrame = pd.DataFrame()
        intervals = []
        for index, row in df.iterrows():
            textgrid = row[textgridColumn]
            annotations = textgrid.get_tier_by_name(tier)._get_annotations()
            intervals.append([float(textgrid.start_time), float(textgrid.end_time)])
        df['Interval'] = pd.Series(intervals)
        self.set_output("result_dFrame", df)



class ComputeFormantsWRASSP(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="intervals", signature="basic:String")]
    _output_ports = [OPort(name="dataFrame", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        df = self.get_input("dataFrame")
        result_dFrame = df
        f1_column = []
        f2_column = []
        f3_column = []
        for index, row in result_dFrame.iterrows():
            formantMap = speech.getFormantsWRASSP(row['WavPath'], row['Interval'][0], row['Interval'][1])
            f1_column.append(formantMap.get_f1())
            f2_column.append(formantMap.get_f2())
            f3_column.append(formantMap.get_f3())
        result_dFrame = result_dFrame.assign(f1=pd.Series(f1_column))
        result_dFrame = result_dFrame.assign(f2=pd.Series(f2_column))
        result_dFrame = result_dFrame.assign(f3=pd.Series(f3_column))
        self.set_output("dataFrame", result_dFrame)



class FormMapWRASSP(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame")]
    _output_ports = [OPort(name="dataFrame", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        df = self.get_input("dataFrame")
        df['Formant_Map'] = df.apply(lambda row: speech.getFormantsWRASSP(row['WavPath'], row['TextGrid'].start_time, row['TextGrid'].end_time), axis=1)
        self.set_output("dataFrame", df)



class getFormantTrajectories(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="targetPhones", signature="basic:List")]
    _output_ports = [OPort(name="dataFrame", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        df = self.get_input("dataFrame")
        targetPhones = set(self.getOptionalInput("targetPhones",[]))
        result_dFrame = pd.DataFrame()
        for index, row in df.iterrows():
            textGrid = row['TextGrid']
            targetsPresent = list(set([str(interval.text) for interval in textGrid.get_tier_by_name('PHN')]))
            if (len(targetPhones) > 0):
                targetsPresent = list(set(targetsPresent) & targetPhones)
            formantMap = row['Formant_Map']
            for target in targetsPresent:
                trajectories = map(list, zip(*speech.getPhoneTrajectory_fromTextgrid(textGrid, formantMap, target, 'PHN')))
                newRow = row
                newRow['Phone'] = target
                newRow['f1_traj'] = trajectories[0][0]
                newRow['f2_traj'] = trajectories[1][0]
                newRow['f3_traj'] = trajectories[2][0]
                result_dFrame = result_dFrame.append(newRow)
        self.set_output("dataFrame", result_dFrame)

    def getOptionalInput(self, inputName, defaultInput):
        try:
            return self.get_input(inputName)
        except:
            return defaultInput



class removeZerosValues(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="targetIndexList", signature="basic:List")]
    _output_ports = [OPort(name="df_all", signature="PandasDataFrame"),
                     OPort(name="df_resultOnly", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        df = self.get_input("dataFrame")
        additionalColumns = pd.DataFrame()
        targetIndexList = self.get_input("targetIndexList")
        for target in targetIndexList:
            df[target+'_noZeros'] = df[target].apply(lambda x: self.removeZerosFromSeries(x))
            additionalColumns[target+'_noZeros'] = df[target].apply(lambda x: self.removeZerosFromSeries(x))
        self.set_output("df_all", df)
        self.set_output("df_resultOnly", additionalColumns)

    def removeZerosFromSeries(self, series):
        return [y for y in series if y>0]




class remove_lowLOD(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="threshold", signature="basic:Integer"),
                    IPort(name="targetIndexList", signature="basic:List")]
    _output_ports = [OPort(name="df_result", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        df_result = self.get_input("dataFrame")
        for targetIndex in self.get_input("targetIndexList"):
            df_result = df_result[df_result[targetIndex].map(len) > self.get_input("threshold")]
        self.set_output("df_result", df_result)




class normalize(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="targetIndexList", signature="basic:List"),
                    IPort(name="factor", signature="basic:Integer")]
    _output_ports = [OPort(name="df_all", signature="PandasDataFrame"),
                    OPort(name="df_resultOnly", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        result_dFrame = self.get_input("dataFrame")
        targetIndexList = self.get_input("targetIndexList")
        factor = self.get_input("factor")
        additionalColumns = pd.DataFrame()
        for target in targetIndexList:
            result_dFrame[target + '_normalized'] = result_dFrame[target].apply(lambda x: speech.normalizeFormantTimeSeries_Interpolate(x, factor))
            additionalColumns[target + '_normalized'] = result_dFrame[target].apply(lambda x: speech.normalizeFormantTimeSeries_Interpolate(x, factor))
        self.set_output("df_all", result_dFrame)
        self.set_output("df_resultOnly", additionalColumns)




class dct_lowGran(Module):
    _input_ports = [IPort(name="inList", signature="basic:List"),
                    IPort(name="depth", signature="basic:Integer")]
    _output_ports = [OPort(name="dct", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        formant = self.get_input("inList")
        depth = self.get_input("depth")
        dctCombination = fftpack.dct(formant)[0:depth].tolist()
        self.set_output("dct", dctCombination)



class get_traj_dct_coefficients(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="targetIndexList", signature="basic:List"),
                    IPort(name="depth", signature="basic:Integer")]
    _output_ports = [OPort(name="dataFrame", signature="PandasDataFrame")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        input_dFrame = self.get_input("dataFrame")
        result_dFrame = input_dFrame
        result_dFrame['dct'] = result_dFrame.apply(self.removeZerosFromSeries, axis=1)
        self.set_output("dataFrame", result_dFrame)

    def removeZerosFromSeries(self, row):
        targetIndexList = self.get_input("targetIndexList")
        depth = self.get_input("depth")
        dctCombination = []
        for target in targetIndexList:
            dctCombination = dctCombination + fftpack.dct(row[target])[0:depth].tolist()
        return dctCombination




class roundRobin(Module):
    _input_ports = [IPort(name="dataFrame", signature="PandasDataFrame"),
                    IPort(name="variableID", signature="basic:String"),
                    IPort(name="dataID_In", signature="basic:String"),
                    IPort(name="dataID_Out", signature="basic:String"),
                    IPort(name="classifierConstructor", signature="PandasDataFrame")]
    _output_ports = [OPort(name="results_byFactor", signature="basic:List"),
                     OPort(name="results_All", signature="basic:List")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        dataFrame = self.get_input("dataFrame")
        factorID = self.get_input("variableID")
        dataIn = self.get_input("dataID_In")
        dataOut = self.get_input("dataID_Out")
        factorValues = dataFrame[factorID].unique().tolist()
        classifiers = [GaussianNB() for factor in factorValues]
        results = []
        for i in range(0,len(factorValues)):
            trainingData = [list(dataFrame[dataFrame[factorID] != factorValues[i]][dataIn]), list(dataFrame[dataFrame[factorID] != factorValues[i]][dataOut])]
            testData = [list(dataFrame[dataFrame[factorID] == factorValues[i]][dataIn]), list(dataFrame[dataFrame[factorID] == factorValues[i]][dataOut])]
            classifiers[i].fit(trainingData[0], trainingData[1])
            predictions = classifiers[i].predict(testData[0])
            results.append([predictions, testData[1]])

        allInputs = [item for sublist in [x[0] for x in results] for item in sublist]
        allOutputs = [item for sublist in [x[1] for x in results] for item in sublist]
        self.set_output("results_byFactor", results)
        self.set_output("results_All", [allInputs, allOutputs])




class generateConfusionMatrix(Module):
    _input_ports = [IPort(name="classificationResults [[expected, result]]", signature="basic:List"),
                    IPort(name="targetPhones", signature="basic:List"),
                    IPort(name="displayValuesOnChart", signature="basic:Boolean")]
    _output_ports = [OPort(name="confusionMatrix", signature="basic:List"),
                     OPort(name="confusionMatrixFigure", signature="matplotlib:MplImshow")]

    def __init__(self):
        Module.__init__(self)

    def compute(self):
        targetPhones = set(self.get_input("targetPhones"))
        classificationResults = self.get_input("classificationResults [[expected, result]]")
        displayValuesOnChart = self.get_input("displayValuesOnChart")
        confusionMatrix = speech.buildConfusionMatrix(classificationResults[0], classificationResults[1], targetPhones)
        self.set_output("confusionMatrix", confusionMatrix[1])
        self.generatePlot(confusionMatrix, displayValuesOnChart)

    def generatePlot(self, confusionMatrix, displayValuesOnChart):
        plt.figure(figsize=(8, 8))
        row_labels = list(confusionMatrix[0])
        plt.xticks(range(len(row_labels)), row_labels, size='medium')
        plt.yticks(range(len(row_labels)), row_labels, size='medium')
        if displayValuesOnChart:
            for row in range(0, len(confusionMatrix[1])):
                for col in range(0, len(row_labels)):
                    plt.text(col, row, confusionMatrix[1][row][col], horizontalalignment='center', verticalalignment='center', size='medium')
        plt.imshow(confusionMatrix[1], cmap='Reds', interpolation='nearest')
        plt.show()





###########################################################################
###########################################################################
###########################################################################




_modules = [PandasDataFrame, TextGrid, VistrailsTextGrid, Create_dFrame, dFrame_toRows, row_to_list, dFrame_row_element, merge_dFrame, Query_dFrame, dFrame_addColumn, dFrame_getUnique, dFrame_getColumn, Length, Concat, Append, Extend, distribute, flatten, ZipList, removeElement, normalizeList, get_annotations, get_interval, removeElement, compare, prepareInputData, FormMapWRASSP, getFormantTrajectories, removeZerosValues, remove_lowLOD, normalize, get_traj_dct_coefficients, dct_lowGran, roundRobin, generateConfusionMatrix, ComputeFormantsWRASSP, getIntervals, getTextgridStartFinishIntervals]

