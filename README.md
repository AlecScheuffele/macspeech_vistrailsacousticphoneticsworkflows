# README #
This repository contains source files used for my "Modeling Speech Analysis Workflows in VisTrails" project conducted at Macquarie University


Jupyter Notebook:
This is the purely python-source version of the analysis task.  The file paths will need to be changed, and a copy of the original sample-data is needed to repeat the workflow execution.


VisTrails Workflows:
These are the XML workflow representations (high granularity and low granularity) referred to in the project report.  These can be imported into VisTrails and opened, but they require the Acoustic Phonetics package to be enabled.


VisTrails Package:
This is the VisTrails acoustic phonetics package.  It contains the modules needed for the VisTrails workflows in this repository.  It can be enabled in VisTrails in (Preferences->Packages).  Further instruction on how to enable a custom VisTrails package can be found in the VisTrails Documentation